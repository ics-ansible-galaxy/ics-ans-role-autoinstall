ics-ans-role-autoinstall
===================

Ansible role to install autoinstall server.

The server will collect Mac addresses based on the autoinstall_scope variable.
Copy a list of grub boot menu (defined in autoinstall_grub_menus)

The grub menu can have options to call a kickstart job (define in each grub menu template)

The kickstart jobs trigger an AWX callback job to revert the boot option to localboot and to download a firstboot script (executed once at the nex localboot of the machine)

The firstboot script callback an AWX template (defined by the autoinstall_post_reboot_job variable)


CSentry has to call the ics-ans-autoinstall/setboot.yml playbook and specify the boot_profile variable to force a machin to boot in the specified install mode.
This boot_profile variable must match a value of autoinstall_grub_menus

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
autoinstall_scope: "for which network group you want to create boot profile (MAC addr symlink)"1
autoinstall_grub_menus: " define a list of grub menus to deploy on the autoinstall server. these menu define which kickstart to use (or not)"
autoinstall_default_bootmode: "define wich boot menu for the host (define a symlink to a grub menu file per MAC addr), the role define a default value, can be overwritten by the invertory"
setboot_job: " which AWX template to call to restore the boot to local at the end of kickstart"
autoinstall_kickstart_files: list of kickstart files to deploy on the autoinstall server
autoinstall_post_reboot_job:  "define wich AWX template to call after the first reboot of the host"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-autoinstall
```

License
-------

BSD 2-clause
