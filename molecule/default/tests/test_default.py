import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-autoinstall-default')


def test_nginx(host):
    service = host.service("nginx")
    assert service.is_running
    assert service.is_enabled
